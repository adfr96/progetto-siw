package it.uniroma3.service;


import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.repository.AttivitaRepository;

@Transactional
@Service
public class AttivitaService {

	@Autowired
	private AttivitaRepository attivitaRepository;

	public Attivita save (Attivita attivita) {
		return this.attivitaRepository.save(attivita);
	}

	public List<Attivita> findByNome(String nome) {
		return this.attivitaRepository.findByNome(nome);
	}

	public Attivita findById(Long id) {
		Optional<Attivita> attivita = this.attivitaRepository.findById(id);
		if (attivita.isPresent()) 
			return attivita.get();
		else
			return null;
	}

	public List<Attivita> findByDataOraAndCentro(Date data,Centro centro) {
		return this.attivitaRepository.findByDataOraAndCentro(data, centro);
	}

	public List<Attivita> findByCentro(Centro centro) {
		return this.attivitaRepository.findByCentro(centro);
	}
	public List<Attivita> findAll() {
		return (List<Attivita>)this.attivitaRepository.findAll();
	}

	public boolean alreadyExists(Attivita attivita) {
		List<Attivita> attivitas= this.attivitaRepository.
				findByNomeAndDataOraAndCentro(attivita.getNome(), attivita.getData_ora(), attivita.getCentro());
		if (attivitas.size() > 0)
			return true;
		else 
			return false;
	}

}
