package it.uniroma3.model;

import java.sql.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


@Entity
public class Attivita {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String nome;
	
	//TODO: non funziona la gestione dell'orario per l'attivita c' è incompatibilità tra il tipo html datetime-local e java
	//@Temporal(TemporalType.TIMESTAMP)
	private Date dataOra;
	
	@ManyToMany
	private Set<Allievo> allievi;
	
	@ManyToOne
	private Centro centro;
	

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData_ora() {
		return dataOra;
	}

	public void setData_ora(Date data_ora) {
		this.dataOra = data_ora;
	}
	public Set<Allievo> getAllievi() {
		return allievi;
	}
	public void setAllievi(Set<Allievo> allievi) {
		this.allievi = allievi;
	}
	public Centro getCentro() {
		return centro;
	}
	public void setCentro(Centro centro) {
		this.centro = centro;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj.getClass() == this.getClass())
		{
			Attivita a =(Attivita) obj;
			return this.id.equals(a.getId());
		}
		return false;
	}
	
	
}
