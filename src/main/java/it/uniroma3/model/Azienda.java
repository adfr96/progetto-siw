package it.uniroma3.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Azienda {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String nome;
	
	private String indirizzo;

	@OneToOne
	private Responsabile direttore;
	
	@OneToMany
	@JoinColumn(name="azienda")
	private List<Responsabile> responsabili;
	
	@OneToMany
	@JoinColumn(name="azienda")
	private List<Allievo> allievi;
	
	@OneToMany
	@JoinColumn(name = "azienda")
	private Set<Centro> centri;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public Responsabile getDirettore() {
		return direttore;
	}

	public void setDirettore(Responsabile direttore) {
		this.direttore = direttore;
	}

	public List<Responsabile> getResponsabili() {
		return responsabili;
	}

	public void setResponsabili(List<Responsabile> responsabili) {
		this.responsabili = responsabili;
	}

	public List<Allievo> getAllievi() {
		return allievi;
	}

	public void setAllievi(List<Allievo>  allievi) {
		this.allievi = allievi;
	}

	public Set<Centro> getCentri() {
		return centri;
	}

	public void setCentri(Set<Centro> centri) {
		this.centri = centri;
	}
	
	
}
