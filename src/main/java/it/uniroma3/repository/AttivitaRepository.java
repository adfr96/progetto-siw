package it.uniroma3.repository;

import org.springframework.data.repository.CrudRepository;

import java.sql.Date;
import java.util.List;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;

public interface AttivitaRepository extends CrudRepository<Attivita, Long>{
	
	List<Attivita> findByNome(String nome);
	List<Attivita> findByDataOraAndCentro(Date data,Centro centro);
	List<Attivita> findByNomeAndDataOraAndCentro(String nome,Date data,Centro centro);
	List<Attivita> findByCentro(Centro centro);
}
