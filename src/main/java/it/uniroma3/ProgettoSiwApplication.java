package it.uniroma3;

import java.util.Calendar;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgettoSiwApplication {

	
	
	public static void main(String[] args) {
		SpringApplication.run(ProgettoSiwApplication.class, args);
		
		/* STAMPA DI PROVA: */
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		System.out.println("***DATA DI OGGI: "+date.toString()+"***");	
	}

}
