package it.uniroma3.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    private final String responsabileQuery = "SELECT username,password,TRUE FROM responsabile WHERE username = ?";
    private final String rolesQuery = "SELECT username,role FROM responsabile WHERE username = ?";

    @Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;
    
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
 
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder())
        		.usersByUsernameQuery(responsabileQuery)
                .authoritiesByUsernameQuery(rolesQuery);
    }
    
    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring()
                .antMatchers( "/assets/css/**","/assets/fonts/**","/assets/js/**","/assets/sass/**","/images/**");
    }
   @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder())
                .usersByUsernameQuery(responsabileQuery)
                .authoritiesByUsernameQuery(rolesQuery);
    }
    
   @Override
   protected void configure(HttpSecurity http) throws Exception {
       http
               .csrf().disable()
               .authorizeRequests()
               .antMatchers("/","/index","/loginSpring").permitAll()
               .anyRequest().permitAll()
               .anyRequest().authenticated()
               .and()
               .formLogin()
               .loginPage("/loginSpring")
               .defaultSuccessUrl("/selectResponsabile")
               .and()
               .logout()
               .logoutSuccessUrl("/loginSpring")
               .permitAll();
   }
}

