package it.uniroma3.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/index").setViewName("index");
		registry.addViewController("/loginSpring").setViewName("loginSpring");
		registry.addViewController("/homeResponsabile").setViewName("azioniResponsabile");
		registry.addViewController("/homeAmministratore").setViewName("azioniAmministratore");
	}
}