package it.uniroma3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import it.uniroma3.model.Azienda;
import it.uniroma3.service.AziendaService;
import it.uniroma3.service.ResponsabileService;

@Controller
public class AziendaController {

	@Autowired
	private AziendaService service;
	
	@Autowired
	private ResponsabileService respService;
	
	@RequestMapping("/monitoraCentri")
	public String viewCentri(Model model) {
		
		model.addAttribute("centri", getAzienda().getCentri());
		return "centriList";
	}
	
	@RequestMapping("/salvaResponsabile/{respId}")
	public String salvaResp(@PathVariable("respId") Long respId) {
		getAzienda().getResponsabili().add(this.respService.findById(respId));
		this.service.save(getAzienda());
		return "registrazioneEffettuata";
	}

	/*cosa spartana per recuperare un'azienda...per ora è una e quindi sti cazzi*/
	private Azienda getAzienda() {
		Azienda azienda = service.findAll().get(0);
		return azienda;
	}
}
