package it.uniroma3.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Centro;

@Component
public class CentroValidator implements Validator{

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
		Centro centro = (Centro) target;
		if(centro.getCapienza() <= 0)
			errors.rejectValue("capienza", "la capienza deve essere positiva");
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Centro.class.equals(clazz);
	}
}
