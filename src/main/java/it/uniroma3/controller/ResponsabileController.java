package it.uniroma3.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import it.uniroma3.controller.validator.ResponsabileValidator;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.ResponsabileService;

@Controller
public class ResponsabileController {

	@Autowired
	private ResponsabileService responsabileservice;
	
	@Autowired
	private ResponsabileValidator responsabileValidator;

	/**
	 * 
	 * @param idResp del responsabile
	 * @param model
	 * @return
	 */
	@RequestMapping("/mostraAttivitaCentro")
	public String mostraAttivitaCentro(@ModelAttribute Long idResp,Model model) {
		Responsabile resp = this.responsabileservice.findById(idResp);
		model.addAttribute("idCentro", resp.getCentro().getId());
		return "/mostraAttivita";
	}

	@RequestMapping(value = "/mostraAttivitaCentroBis" )
	public String mostraAttivitaCentroBis(@RequestParam("idResp") String idResp,Model model) {
		Responsabile resp = this.responsabileservice.findById(new Long(idResp));
		String centro_string_id = resp.getCentro().getId().toString();
		model.addAttribute("idCentro", centro_string_id);
		return "forward:/mostraAttivitaCentro/"+(resp.getCentro().getId());
	}

	@RequestMapping("/showListaResponsabili")
	public String showListaResponsabili(Model model) {
		model.addAttribute("responsabili", this.responsabileservice.findAll());		
		return "responsabiliList";
	}
	
	/*vecchio, in seguito eliminare*/
	@RequestMapping("/selectResponsabile/{idResp}") 
	public String selectResp(@PathVariable("idResp") String idResp, Model model, HttpSession session) {
		Responsabile resp = this.responsabileservice.findById(new Long(idResp));
		session.setAttribute("resp", resp);
		return "azioniAmministratore";
	} 
	
	@RequestMapping("/selectResponsabile") 
	public String selectResponsabile(HttpSession session,Authentication a) {
		User u =(User) a.getPrincipal();
		Responsabile resp = this.responsabileservice.findByUsername(u.getUsername());
		session.setAttribute("resp",resp );
		System.out.println(resp.getRole());
		if(resp.getRole().equals("ADMIN"))
			return "azioniAmministratore";
		else 
		{
			session.setAttribute("centro", resp.getCentro().getNome());
			return "azioniResponsabile";
		}
	}
	@RequestMapping("/toHome") 
	public String toHome(@SessionAttribute("resp") Responsabile resp) {
		if(resp.getRole().equals("ADMIN"))
			return "azioniAmministratore";
		else {
			return "azioniResponsabile";
		}
	}
	
	@RequestMapping(value =  "/toResponsabileForm")
	public String toRespForm(Model model) {
		model.addAttribute("responsabile", new Responsabile());
		return "responsabileForm";
	}
	@RequestMapping(value =  "/addResponsabile")
	public String addResp(@ModelAttribute("responsabile") Responsabile resp,Model model
				, BindingResult bindingResult) {
		this.responsabileValidator.validate(resp, bindingResult);
		
		
		if (this.responsabileservice.alreadyExists(resp)) {
			model.addAttribute("exists", "Responsabile Gia esiste");
			return "responsabileForm";
			
		} 
		else
		{
			if (!bindingResult.hasErrors()) 
			{
				this.responsabileservice.save(resp);
				return "forward:/salvaResponsabile/"+resp.getId();
			}
		}
		return "responsabileForm";
	}
}

