package it.uniroma3.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import it.uniroma3.controller.validator.AllievoValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;

@Controller
public class AllievoController {

	@Autowired
	private AllievoService allievoService;

	@Autowired
	private AllievoValidator validator;
	
	@RequestMapping("/addAllievo")
	public String addAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "allievoForm";
	}
	@RequestMapping("/listAllievi")
	public String listAllievi(Model model) {
		model.addAttribute("allievi", this.allievoService.findAll());
		return "allieviList";
	}
	@RequestMapping(value = "/allievo/{id}", method = RequestMethod.GET)
	public String getAllievo(@PathVariable("id") Long id, Model model) {
		Allievo allievo = this.allievoService.findById(id);
		model.addAttribute("allievo", allievo);
		model.addAttribute("attivitaAllievo", allievo.getAttivita());
		return "showAllievo";
	}
	
	@RequestMapping(value = "/allievo", method = RequestMethod.POST)
	public String newAllievo(@ModelAttribute("allievo") Allievo allievo, 
			Model model, BindingResult bindingResult) {		
		this.validator.validate(allievo, bindingResult);

		if (this.allievoService.alreadyExists(allievo)) {
			model.addAttribute("exists", "Customer already exists");
			return "allievoForm";
			
		}
		else {
			if (!bindingResult.hasErrors()) {
				this.allievoService.save(allievo);
				model.addAttribute("allievi", this.allievoService.findAll());
				return "registrazioneEffettuata";
			}
		}
		return "allievoForm";
	}
	
	/**
	 * newPartecipazione viene richiamato dal metodo di AttivitaController per la registrazione dell'attivita 
	 * su Allievo(List<Attivita> attivita) 
	 * @param allievo
	 * @param attivita
	 * @return 
	 */
	@RequestMapping(value ="partecipazioneAllievo")
	public String newPartecipazione(@SessionAttribute("allievoDaRegistrare") Allievo allievo, @SessionAttribute Attivita attivita,HttpSession session) {
		allievo.getAttivita().add(attivita);
		this.allievoService.save(allievo);
		session.removeAttribute("allievoDaRegistrare");
		session.removeAttribute("listaAttivitaGiornaliere");
		return "registrazioneEffettuata";
	}
	/** 
	 * aggiunge a model un elenco degli allievi per selezionare poi nella pagina (allieviListPrenotazione) 
	 * l'allievo che deve essere registrato
	 * @param model
	 * @param session
	 * @return la pagina HTML allieviListPartecipazione
	 */
	@RequestMapping("/listAllieviPartecipazione") 
	public String showAllievi(Model model, HttpSession session) {
		model.addAttribute("elencoAllievi",this.allievoService.findAll());		
		return "allieviListPartecipazione";
	}
	/**
	 * E'richiamato dalla pagina allieviListPartecipazione, viene passato come parametro l' id dell'Allievo scelto per la registrazione,
	 * viene recuperato dalla sessione il responsabile resp per conoscere l'id del centro di cui è responsabile, viene settato in sessione l'allievo il cui id è idAllievo,
	 * si esegue un forward verso il centro per la gestione delle attivita che in esso vengono svolte
	 * @param idAllievo
	 * @param model
	 * @param resp
	 * @param session
	 * @return  il metodo di CentroController mostraAttivitaPerPartecipazione
	 */
	@RequestMapping("/settaAllievo")
	public String addPartecipazioneAttivita(@RequestParam("idAllievo") String idAllievo,Model model,
			@SessionAttribute("resp") Responsabile resp, HttpSession session) {
		session.setAttribute("allievoDaRegistrare",this.allievoService.findById(new Long(idAllievo)));
		//Long idCentro = resp.getCentro().getId();
		return "forward:/showAttivitaCentro";
	}
}
