package it.uniroma3.controller;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import it.uniroma3.controller.validator.AttivitaValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AttivitaService;

@Controller
public class AttivitaController {

	@Autowired 
	private AttivitaService attivitaService;

	@Autowired
	private AttivitaValidator validator;
	/**
	 * metodo che riceve la richesta di aggiungere un nuova attivita e la rimanda alla form
	 * @param model
	 * @return attivitaForm.html
	 */
	@RequestMapping("/addAttivita")
	public String addAttivita(Model model) {
		model.addAttribute("attivita", new Attivita());
		return "attivitaForm";
	}
	/**
	 * metodo che riceve i dati dalla form, e salva l'attivita, in particolare salva l'attivita nel centro che ha per responsabile
	 * resp, ovvero quello della sessione (responsaile ha effettutato il login)
	 * @param attivita
	 * @param model
	 * @param bindingResult
	 * @return attivitaForm.html se ci sono errori nell'inserimento dei dati
	 * @return attivitaList.html se è andato tutto bene
	 */
	@RequestMapping(value = "/attivita", method = RequestMethod.POST)
	public String newAttivita(@ModelAttribute("attivita") Attivita attivita, 
			Model model, BindingResult bindingResult,@SessionAttribute("resp") Responsabile resp) {
		this.validator.validate(attivita, bindingResult);

		if (this.attivitaService.alreadyExists(attivita)) {
			model.addAttribute("exists", "QUESTA ATTIVITA E' GIA STATA INSERITA");
			return "attivitaForm";
		}
		else {
			if (!bindingResult.hasErrors()) {
				attivita.setCentro(resp.getCentro());
				this.attivitaService.save(attivita);
				model.addAttribute("attivitaCentro", this.attivitaService.findByCentro(resp.getCentro()));
				return "attivitaList";
			}
		}
		return "attivitaForm";
	}
	
	/**
	 * trova la lista di tutte le attività e le mostra
	 * @param model
	 * @return attivitaList.html
	 */
	@RequestMapping("/listAttivita")
	public String listAttivita(Model model,@SessionAttribute("resp") Responsabile resp) {
		model.addAttribute("attivitaCentro", this.attivitaService.findByCentro(resp.getCentro()));
		return "attivitaList";
	}
	/**
	 * trova un'attivita dal suo id e inoltra alla pagina che visualizza le sue informazione dettagliate
	 * @param id
	 * @param model
	 * @return showAttivita.html
	 */
	@RequestMapping(value = "/attivita/{id}", method = RequestMethod.GET)
	public String getAttivita(@PathVariable("id") Long id, Model model) {
		model.addAttribute("attivita", this.attivitaService.findById(id));
		return "showAttivita";
	}

	/** newPartecipazione viene richiamato dalla pagina HTML sceltaAttivita, per inserire sull'attivita (List<Allievo> allievi)
	 * l'allievo che vi partecipa  
	 * @param idAttivita
	 * @param allievoDaRegistrare
	 * @param session
	 * @return forward: partecipazioneAllievo( metodo di AllievoController per inserire l'attivita scelta sulla lista delle attivita dell'allievo)
	 */
	@RequestMapping(value = "/partecipazioneAttivita", method = RequestMethod.POST)
	public String newPartecipazione(@RequestParam ("idAttivita") String idAttivita,@SessionAttribute("allievoDaRegistrare") Allievo allievoDaRegistrare, 
			HttpSession session,Model model) {		
		/* 
		Equivalente a passare @SessionAttribute
		HttpSession session = request.getSession();
		Allievo allievo = (Allievo)session.getAttribute("allievo");
		 */

		Attivita attivita = this.attivitaService.findById(new Long(idAttivita));
		if(attivita.getAllievi().contains(allievoDaRegistrare))
		{
			model.addAttribute("exists", "l'allievo si è gia registrato all'attivita");
			return "forward:/showAttivitaCentro";
		}
		attivita.getAllievi().add(allievoDaRegistrare);
		this.attivitaService.save(attivita);
		session.setAttribute("attivita", attivita);
		return "forward:/partecipazioneAllievo/";
	}

	@RequestMapping("/getAttivitaPerDataCentro")
	public String getAttivitaPerDataCentro(@SessionAttribute("centro") Centro centro, Model model,HttpSession session) {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		List<Attivita> attivitaGiornaliere = this.attivitaService.findByDataOraAndCentro(date, centro);
		
		int numIscrittiOggi = 0;
		for ( Attivita attivita: attivitaGiornaliere) {
			numIscrittiOggi += attivita.getAllievi().size();
		}

		if(numIscrittiOggi < centro.getCapienza())
		{
			session.setAttribute("listaAttivitaGiornaliere", attivitaGiornaliere);
			return "forward:listAllieviPartecipazione";
		}
		else {
			model.addAttribute("capienzaMax", "Spiacenti, ma per oggi è stata raggiunta la capienza massima!!!");
			return "azioniResponsabile";
		}
	}
}
