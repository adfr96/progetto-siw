package it.uniroma3.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Attivita;
import it.uniroma3.service.CentroService;

@Controller
public class CentroController {

	@Autowired
	private CentroService centroService;
	/**
	 * metodo che recupera la lista delle attivita del centro, 
	 * e inoltra alla pagina per la visulaizzazione dettagliata
	 * @param id del centro
	 * @param model
	 * @return attivitaListDettagliata.html
	 */
	@RequestMapping(value = "/mostraAttivitaCentro/{idCentro}")
	public String mostraAttivita(@PathVariable Long idCentro,Model model) {
		Centro c = centroService.findById(idCentro);
		model.addAttribute("attivitaCentro", c.getAttivita());
		return "attivitaListDettagliata";
	}

	/**
	 * Viene richiamato da addPartecipazioneAttivita in ResponsabileController, cerca le attivita associate al centro il cui id (idCentro),
	 * è passato come parametro, richiama la pagina HTML sceltaAttivita per la scelta dell'attivita tra quelle offerte dal centro a cui registrare l'allievo
	 * @param idCentro
	 * @param model
	 * @return la pagina HTML sceltaAttivita
	 */
	@RequestMapping(value = "/showAttivitaCentro")
	public String mostraAttivitaPerPartecipazione(@SessionAttribute("centro") Centro centro, 
			@SessionAttribute("listaAttivitaGiornaliere") List<Attivita> attivitaGiornaliere, Model model) {
		
		/*stampa debug*/
		for(Attivita  a : attivitaGiornaliere)
		{
			System.out.println(a.getNome());
		}
		
		model.addAttribute("elencoAttivita",attivitaGiornaliere);
		return "sceltaAttivita";
	}

	@RequestMapping(value = "/verificaCapienza")
	public String addAllievo(@SessionAttribute("resp") Responsabile resp, Model model,HttpSession session) {
		Centro c = resp.getCentro();
		session.setAttribute("centro", c);
		return "forward:getAttivitaPerDataCentro";
	}
}
